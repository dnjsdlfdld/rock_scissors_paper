import 'dart:math';
import 'package:flutter/material.dart';

class PageGame extends StatefulWidget {
  const PageGame({super.key, required this.title});

  final String title;

  @override
  State<PageGame> createState() => _PageGameState();
}

class _PageGameState extends State<PageGame> {
  int _gamePoint = 0;
  int _countWin = 0;
  int _countDraw = 0;
  int _countLose = 0;
  String _gameMessage = "";

  int? _myChoice = null; // ? > 있어도 되고 없어도 되고 널허용
  int? _comChoice = null;

  final List<Map> _gameItem = [
    { 'name' : '가위', 'imgSrc' : 'assets/가위.jpg' },
    { 'name' : '바위', 'imgSrc' : 'assets/바위.jpg' },
    { 'name' : '보', 'imgSrc' : 'assets/보자기1.jpg' },
  ];

  void _gameStart(int itemIndex) {
    setState(() {
      if (_gamePoint < 1000) {
        _gameMessage = "포인트가 부족합니다. 포인트를 충전해주세요";
      } else {
        _myChoice = itemIndex;
        _comChoice = Random().nextInt(_gameItem.length);

        if ((_myChoice == 0 && _comChoice == 1) || (_myChoice == 1 && _comChoice == 2)
            || (_myChoice == 2 && _comChoice == 0)
        ) {
          _countLose += 1;
          _gamePoint -= 1000;
          _gameMessage = "패배... 1000포인트 잃었습니다.";

        } else if ((_myChoice == 0 && _comChoice == 2) || (_myChoice == 1 && _comChoice == 0) ||
            (_myChoice == 2 && _comChoice == 1)
        ) {
          _countWin += 1;
          _gamePoint += 1000;
          _gameMessage = "승리... 1000포인트 획득했습니다";

        } else {
          _countDraw +=1;
          _gameMessage = "무승부... 제대로 하세요";
        }
      }
    });
  }

  void _pointCharge() {
    setState(() {
      _gamePoint += 10000;
    });
  }


  void _gameReset() {
    setState(() {
      _gamePoint = 0;
      _countWin = 0;
      _countDraw = 0;
      _countLose = 0;
      _gameMessage = "";

      _myChoice = null;
      _comChoice = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [ // 여러 줄의 컬럼이 들어가니까
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Point : $_gamePoint', style: TextStyle(fontSize: 30),),
              OutlinedButton(onPressed: () {
                _pointCharge();
              }, child: Text('포인트 충전'))
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text('승리 : $_countWin'),
              Text('무승부 : $_countDraw'),
              Text('패배 : $_countLose'),
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center,
            children: [
              OutlinedButton(
                  onPressed: () {
                    _gameStart(0);
                  }, child: Text('가위')),

              OutlinedButton(
                  onPressed: () {
                    _gameStart(1);
                  }, child: Text('바위')),

              OutlinedButton(
                  onPressed: () {
                    _gameStart(2);
                  }, child: Text('보')),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text('나'),
                  Image.asset(
                    "${_myChoice == null ? 'assets/짱구.jpg' : _gameItem[_myChoice!]['imgSrc']}",
                    width: 70,
                    height: 70,
                  )
                ],
              ),
              Text('VS'),
              Column(
                children: [
                  Text('COM'),
                  Image.asset(
                    "${_comChoice == null ? 'assets/짱구.jpg' : _gameItem[_comChoice!]['imgSrc']}",
                    width: 70,
                    height: 70,
                  ),
                ],
              ),
            ],
          ),
          Text('$_gameMessage'),
          OutlinedButton(onPressed: () {
            _gameReset();
          }, child: Text('Reset'))
        ],
      ),
    );
  }
}
