import 'package:flutter/material.dart';
import 'package:rock_scissors_paper/page_game.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: const PageGame(title: '가위바위보 게임'),
    );
  }
}
